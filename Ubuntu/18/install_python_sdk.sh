#!/bin/bash

################################################################################
# Prerequisites:
# - .NET Core
#
# The following scripts will setup OpenTAP, Python 3.7 and Python Plugin
# https://packages.opentap.io/index.html#/?name=Python
#
################################################################################

GREEN="\033[0;32m"
RED="\033[0;33m"
ON_GREEN="\033[42m"
ON_RED="\033[41m"
On_IGREEN="\033[0;102m"
IGREEN="\033[0;92m"
NC="\033[0m"

# Check if .NET Core is installed, else prompt user to install
DOTNET_PATH="/usr/share/dotnet/shared/Microsoft.NETCore.App"
if [ ! -d "$DOTNET_PATH" ]; then
  wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
  sudo dpkg -i packages-microsoft-prod.deb
  rm packages-microsoft-prod.deb
  sudo apt-get update
  sudo apt-get install -y apt-transport-https
  sudo apt-get update
  sudo apt-get install -y dotnet-sdk-2.1
  if [ ! -d "$DOTNET_PATH" ]; then
    echo -e "${ON_RED}Installation of dotnet 2.1 failed!${NC}"
    exit 1
  fi
fi
DOTNET_VER=$(ls $DOTNET_PATH | tr "\n" "," | sed "s/,$//" )
echo -e "${ON_GREEN}Found .NET Core [${DOTNET_VER}]${NC}"

# Installing OpenTAP if it is not yet installed
OPENTAP_PATH="$HOME/.tap"
if [ ! -d $OPENTAP_PATH ]; then
  echo "Installing OpenTAP..."
  OPENTAP_LATEST=$(wget -q -O - https://opentap.io/download.html | grep -o "\".*\.tar\"" | tr -d '"')
  [ -d opentap_setup ] && rm -rf opentap_setup
  mkdir opentap_setup
  pushd ./ > /dev/null
  cd opentap_setup
  wget -nv -O opentap.latest.tar https://opentap.io/${OPENTAP_LATEST}
  tar -xf opentap.latest.tar
  chmod u+x ./INSTALL.sh
  yes | ./INSTALL.sh
  popd
fi
source ~/.profile
TAP=$(which tap)
OPENTAP_VER=$(${TAP} -h | head -n1 | grep -oP "(?<=\().*(?=\))")
echo -e "${ON_GREEN}Found OpenTAP version [${OPENTAP_VER}]${NC} "

# Installing python3.7
sudo apt install -y python3.7 python3.7-dev
python3.7 -m pip install pip
# sudo ln -s /usr/bin/python3.7 /usr/bin/python
PYTHON_VER=$(python3.7 --version | grep -oP "(\d.*)")
PYTHON_VER_MAJOR_MIN=$(python3.7 --version |  grep -oP "(\d\.\d)")
echo -e "${ON_GREEN}Found Python ${PYTHON_VER_MAJOR_MIN} [${PYTHON_VER}]${NC}"

# Installing OpenTAP Python SDK Plugin
$TAP package install Python
if [ $? -ne 0 ]; then
  echo -e "${ON_RED}Unable to install OpenTAP Python SDK!${NC}"
  exit 2
fi

# Setup Python SDK for OpenTAP
PYTHON_DYNLIB="/usr/lib/python${PYTHON_VER_MAJOR_MIN}/config-${PYTHON_VER_MAJOR_MIN}m-x86_64-linux-gnu/libpython${PYTHON_VER_MAJOR_MIN}.so"
PYTHON_PLUGIN="/usr/local/lib/pythonplugin"
PYTHON_PLUGIN_LIB="/usr/local/lib/pythonplugin/libpython${PYTHON_VER_MAJOR_MIN}.so"
[ -f ${PYTHON_PLUGIN_LIB} ] && sudo rm ${PYTHON_PLUGIN_LIB}
[ ! -d ${PYTHON_PLUGIN} ] && sudo mkdir -p ${PYTHON_PLUGIN}
sudo ln -s $PYTHON_DYNLIB ${PYTHON_PLUGIN_LIB}
$TAP python set-path ${PYTHON_PLUGIN} ${PYTHON_VER_MAJOR_MIN}
if grep -q LD_LIBRARY_PATH ~/.bashrc; then 
   echo $LD_LIBRARY_PATH
else
   echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${PYTHON_PLUGIN}" >> ~/.bashrc
   echo "export LD_LIBRARY_PATH" >> ~/.bashrc
   source ~/.bashrc
fi
