This project contains scripts to automate installation and setup of various OpenTAP plugins that may require different prerequisites in Linux OSes.

OpenTAP Plugins are available at: https://packages.opentap.io/index.html
